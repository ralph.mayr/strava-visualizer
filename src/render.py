from PIL import Image, ImageDraw, ImageFont, ImageColor, ImageOps
import os
import io
import json
import calendar
import dateutil.parser
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from numpy import where
from numpy import unique
from numpy import transpose
from matplotlib import pyplot
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.mixture import GaussianMixture
from plotly.subplots import make_subplots
from sklearn.preprocessing import MinMaxScaler
from activities import GetActivities, GetActivity

pd.options.plotting.backend = "plotly"

DATA_DIR = "data"
IMAGES_DIR = "images"

CANVAS_SIZE = 640
HEADLINE_AREA_HEIGHT = int(CANVAS_SIZE / 7)
OUTER_PADDING = 16
LARGE_FONT_SIZE = 40
MEDIUM_FONT_SIZE = 26
SMALL_FONT_SIZE = 18
ICON_SIZE = SMALL_FONT_SIZE
PADDING = 8

FONT_REGULAR = "fonts/Raleway-Light.ttf"
FONT_BOLD = "fonts/Raleway-Bold.ttf"

LARGE_FONT = ImageFont.truetype(font=FONT_BOLD, size=LARGE_FONT_SIZE)
MEDIUM_FONT = ImageFont.truetype(font=FONT_REGULAR, size=MEDIUM_FONT_SIZE)
SMALL_FONT = ImageFont.truetype(font=FONT_REGULAR, size=SMALL_FONT_SIZE)

PRIMARY_COLOR = '#FF6E31'
TOP_BG_COLOR = "#3C6255"

OVERVIEW_ICON_COLOR = "#000000"

HEADLINE_COLOR = "#ffffff"
SUBHEADLINE_COLOR = "#ffffff"

MAIN_COLOR_SEQUENCE = [PRIMARY_COLOR, '#FFEBB7', '#243763', '#AD8E70']

HR_ZONE_COLOR_SEQUENCE = ['white', '#F7F5EB',
                          '#D7E9B9', '#FFD56F', '#FFB26B', '#FF7B54']
HR_LINE_COLOR = '#6C00FF'
HR_ZONES = [93, 110, 129, 147, 166, 200]

ALTITUDE_LINE_COLOR = '#FFBABA'

ICONS = {}

def CreateBackground() -> Image:
    result = Image.new(mode="RGBA", size=(
        CANVAS_SIZE, CANVAS_SIZE), color="white")
    draw = ImageDraw.Draw(result)

    draw.rectangle((0, 0, CANVAS_SIZE, HEADLINE_AREA_HEIGHT), TOP_BG_COLOR)

    # draw.polygon(
    #     [
    #         (int(.1*CANVAS_SIZE), CANVAS_SIZE),
    #         (int(.3*CANVAS_SIZE), int(.8*CANVAS_SIZE)),
    #         (int(.5*CANVAS_SIZE), int(.6*CANVAS_SIZE)),
    #         (int(.8*CANVAS_SIZE), int(.65*CANVAS_SIZE)),

    #         (CANVAS_SIZE, int(.3*CANVAS_SIZE)),
    #         (CANVAS_SIZE, CANVAS_SIZE)
    #     ],
    #     fill='#c1d3cd'
    # )

    # logoWidth = int(CANVAS_SIZE / 20)
    # logo = ImageOps.contain(Image.open(
    #     'assets/favicon.png'), (logoWidth, logoWidth))

    # result.paste(logo, box=[CANVAS_SIZE - logoWidth -
    #              16, CANVAS_SIZE - logo.size[1] - 16])

    return result


def RecolorIcon(icon, rgb):
    data = np.array(icon)   # "data" is a height x width x 4 numpy array
    red, green, blue, alpha = data.T  # Temporarily unpack the bands for readability

    black_areas = (red == 0) & (blue == 0) & (green == 0)
    data[..., :-1][black_areas.T] = rgb  # Transpose back needed

    return Image.fromarray(data)


def LoadIcons():
    primaryColor = ImageColor.getcolor(OVERVIEW_ICON_COLOR, "RGB")

    icon_run = Image.open('icons/run.png')
    icon_run = icon_run.crop(
        (100, 100, icon_run.width - 100, icon_run.height - 100))
    icon_run = icon_run.resize((ICON_SIZE, ICON_SIZE))
    icon_run = RecolorIcon(icon_run, primaryColor)

    icon_ride = Image.open('icons/ride.png')
    icon_ride = icon_ride.crop(
        (100, 100, icon_ride.width - 100, icon_ride.height - 100))
    icon_ride = icon_ride.resize((ICON_SIZE, ICON_SIZE))
    icon_ride = RecolorIcon(icon_ride, primaryColor)

    icon_workout = Image.open('icons/workout.png')
    icon_workout = icon_workout.crop(
        (100, 100, icon_workout.width - 100, icon_workout.height - 100))
    icon_workout = icon_workout.resize((ICON_SIZE, ICON_SIZE))
    icon_workout = RecolorIcon(icon_workout, primaryColor)

    icon_walk = Image.open('icons/walk.png')
    icon_walk = icon_walk.crop(
        (100, 100, icon_walk.width - 100, icon_walk.height - 100))
    icon_walk = icon_walk.resize((ICON_SIZE, ICON_SIZE))
    icon_walk = RecolorIcon(icon_walk, primaryColor)

    ICONS["run"] = icon_run
    ICONS["ride"] = icon_ride
    ICONS["workout"] = icon_workout
    ICONS["walk"] = icon_walk


def GetIcon(icon, size, color):
    result = Image.open('icons/' + icon + '.png')
    result = result.crop(
        (100, 100, result.width - 100, result.height - 100))
    result = RecolorIcon(result, color)
    result = result.resize((size, size))
    return result


def DrawActivitiesForMonth(result, month, activities, topOffset=0):
    leftOffset = 2*OUTER_PADDING
    left = leftOffset
    top = PADDING + topOffset

    ImageDraw.Draw(result).text((left, top - 5), month,
                                font=SMALL_FONT, fill=OVERVIEW_ICON_COLOR)

    left += ICON_SIZE + 4 * PADDING

    idx = 0
    for activity in activities:
        result.paste(ICONS[activity], box=[left, top], mask=ICONS[activity])
        left += ICON_SIZE + PADDING
        if (idx < len(activities) - 1 and left > CANVAS_SIZE - ICON_SIZE - OUTER_PADDING):
            left = leftOffset + ICON_SIZE + 4 * PADDING
            top += ICON_SIZE + PADDING
        idx += 1

    return top + PADDING * 2


LoadIcons()


def GetActivityMonthName(activity) -> str:
    month = dateutil.parser.isoparse(activity["start_date"]).strftime('%b')
    return month


def GetActivityMonthNumber(activity) -> int:
    month = dateutil.parser.isoparse(activity["start_date"]).month
    return month


def GetActivityDate(activity) -> str:
    date = dateutil.parser.isoparse(
        activity["start_date_local"]).strftime('%a, %d.%m.%Y @ %H:%M')
    return date


def GetActivityMovingTime(activity) -> str:
    time = activity["moving_time"]
    return TimeToString(time)


def RenderOverview(year) -> Image:
    result = CreateBackground()
    stats = GetStats(year)

    subHeadline = "{} ACTIVITIES | {} ACTIVE DAYS".format(
        stats['totalActivities'], stats['totalActiveDays'])

    topOffset = PasteHeadlines(result, str(year), subHeadline, "chart")

    for filename in sorted(os.listdir(DATA_DIR)):
        f = os.path.join(DATA_DIR, filename)

        if os.path.isfile(f) and filename.startswith("{}-".format(year)) and filename.endswith(".json"):
            data = json.load(open(f))

            month = "n/a"
            if (len(data) > 0):
                month = GetActivityMonthName(data[0])

            activity_types = []

            sortedData = sorted(data, key=lambda x: x['start_date'])

            for activity in sortedData:
                match activity["type"]:
                    case "Run":
                        activity_types.append("run")
                    case "Ride":
                        activity_types.append("ride")
                    case "Workout":
                        activity_types.append("workout")
                    case "Walk":
                        activity_types.append("walk")
                    case _:
                        print("No match for " + activity["type"])

            if len(activity_types) > 0:
                topOffset = DrawActivitiesForMonth(
                    result, month, activity_types, topOffset)

    result.save(IMAGES_DIR + "/overview-{}.png".format(year), "PNG")

    return result


def TimeToString(timeInSeconds) -> str:
    totalHours = timeInSeconds // 3600
    totalSeconds = timeInSeconds % 3600
    totalMinutes = totalSeconds // 60

    return "{}h{}m".format(totalHours, totalMinutes)


def GetStats(year):
    totalActivities = 0
    totalMovingTime = 0
    movingTimeRun = 0
    distanceRun = 0
    elevationRun = 0
    countRuns = 0
    uniqueDays = set()

    for filename in sorted(os.listdir(DATA_DIR)):
        f = os.path.join(DATA_DIR, filename)

        if os.path.isfile(f) and filename.startswith("{}-".format(year)) and filename.endswith(".json"):
            data = json.load(open(f))

            activity_types = []
            for activity in data:
                day = dateutil.parser.isoparse(
                    activity["start_date"]).strftime('%d%m')
                uniqueDays.add(day)

                totalActivities += 1
                totalMovingTime += activity["moving_time"]
                match activity["type"]:
                    case "Run":
                        activity_types.append("run")
                        distanceRun += activity["distance"]
                        elevationRun += activity["total_elevation_gain"]
                        movingTimeRun += activity["moving_time"]
                        countRuns += 1
                    case "Ride":
                        activity_types.append("ride")
                    case "Workout":
                        activity_types.append("workout")
                    case "Walk":
                        activity_types.append("walk")
                    case _:
                        print("No match for " + activity["type"])

    return {
        "totalActivities": str(totalActivities),
        "totalActiveDays": str(len(uniqueDays)),
        "totalMovingTime": TimeToString(totalMovingTime),
        "movingTimeRun": TimeToString(movingTimeRun),
        "distanceRun": "{} km".format(int(distanceRun // 1000)),
        "elevationRun": "{} m".format(int(elevationRun)),
        "countRuns": countRuns
    }


def PasteHeadlines(result: Image, headline: str, subHeadline: str, icon: str = None) -> float:
    topOffset = int(OUTER_PADDING / 2)
    ImageDraw.Draw(result).text((OUTER_PADDING, topOffset), headline,
                                font=LARGE_FONT, fill=HEADLINE_COLOR)

    topOffset += LARGE_FONT.size
    leftOffset = OUTER_PADDING

    if icon != None:
        iconSize = int(MEDIUM_FONT_SIZE * 1.2)
        renderedIcon = GetIcon(icon, iconSize, (255, 255, 255))

        result.paste(renderedIcon, box=[
                     OUTER_PADDING, topOffset + PADDING], mask=renderedIcon)
        leftOffset += iconSize + PADDING

    ImageDraw.Draw(result).text((leftOffset, topOffset), subHeadline,
                                font=MEDIUM_FONT, fill=SUBHEADLINE_COLOR)

    return HEADLINE_AREA_HEIGHT + PADDING


def PasteFigure(fig, topOffset, result: Image):
    maxHeight = CANVAS_SIZE - int(topOffset / 2)
    img_bytes = fig.to_image(format="png", width=maxHeight, height=maxHeight)
    buf = io.BytesIO(img_bytes)

    chart = Image.open(buf)
    chart = chart.resize((maxHeight, maxHeight))

    result.paste(
        chart, box=[int((CANVAS_SIZE - maxHeight) / 2), int(CANVAS_SIZE - maxHeight)], mask=chart)


def RenderComparison(year, previous) -> Image:
    result = CreateBackground()
    topOffset = PasteHeadlines(result, str(year), "VS. " + str(previous))

    stats = GetStats(year)
    previousStats = GetStats(previous)

    types = [
        {
            "headline": "ACTIVITIES",
            "key": "totalActivities"
        },
        {
            "headline": "ACTIVE DAYS",
            "key": "totalActiveDays"
        },
        {
            "headline": "MOVING TIME",
            "key": "totalMovingTime"
        },
        {
            "headline": "DISTANCE (RUN)",
            "key": "distanceRun"
        },
        {
            "headline": "ELEVATION (RUN)",
            "key": "elevationRun"
        },
    ]

    for type in types:
        ImageDraw.Draw(result).text((OUTER_PADDING, topOffset), type["headline"],
                                    font=SMALL_FONT, fill=(255, 255, 255, 0))
        topOffset += SMALL_FONT.size
        ImageDraw.Draw(result).text((OUTER_PADDING, topOffset), stats[type["key"]],
                                    font=MEDIUM_FONT, fill=(255, 255, 255, 0))
        topOffset += MEDIUM_FONT.size
        ImageDraw.Draw(result).text((OUTER_PADDING,  topOffset), previousStats[type["key"]],
                                    font=MEDIUM_FONT, fill=(255, 255, 255, 128))
        topOffset += SMALL_FONT.size * 2 + PADDING

    result.save(IMAGES_DIR + "/yoy-{}-{}.png".format(year, previous), "PNG")

    return result


def RenderActivityDistributionByMonth(year) -> Image:
    result = CreateBackground()

    stats = GetStats(year)
    subHeadline = '{} ACTIVITIES | {}'.format(
        stats['totalActivities'], stats['totalMovingTime'])

    df = GetDataFrameForActivities(year, activityType='all')

    labels = []
    values = []
    for label in df['Type'].unique():
        labels.append(label)
        sumDuration = df[df['Type'] == label]['Duration'].sum()
        values.append(sumDuration)

    fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
    fig.update_layout(
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)")
    fig.update_traces(
        marker=dict(colors=MAIN_COLOR_SEQUENCE),
        textposition='inside',
        textinfo='label+percent')

    topOffset = PasteHeadlines(result, str(year), subHeadline, "chart")
    PasteFigure(fig, topOffset, result)

    result.save(
        IMAGES_DIR + "/activity-distribution-by-month{}.png".format(year), "PNG")

    return result


def RenderActivityDistributionByTimeOfDay(year) -> Image:
    result = CreateBackground()

    stats = GetStats(year)
    subHeadline = '{} ACTIVITIES | {}'.format(
        stats['totalActivities'], stats['totalMovingTime'])

    df = GetDataFrameForActivities(year, activityType='all')
    df2 = pd.DataFrame({"Start Hour": [], "Type": [], "Count": []})

    for timeOfDay in df['Start Hour'].unique():
        entries = df[df['Start Hour'] == timeOfDay]
        for type in entries['Type'].unique():
            count = len(df[(df['Start Hour'] == timeOfDay)
                        & (df['Type'] == type)])
            df2.loc[len(df2.index)] = [timeOfDay, type, count]

    fig = px.bar(df2, x='Start Hour', y='Count', color='Type',
                 color_discrete_sequence=MAIN_COLOR_SEQUENCE, text_auto=True)

    fig.update_layout(
        xaxis={"title": "", "dtick": 1, "ticksuffix": ":00"}, yaxis={"title": "", "dtick": 25, "showticklabels": True}, showlegend=True,
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)")

    topOffset = PasteHeadlines(result, str(year), subHeadline, "chart")
    PasteFigure(fig, topOffset, result)

    result.save(
        IMAGES_DIR + "/activity-distribution-time-of-day{}.png".format(year), "PNG")

    return result


def RenderDistanceHistogram(year) -> Image:
    result = CreateBackground()

    data = {
        "distance": []
    }

    activities = GetActivities(DATA_DIR, year, type="Run")
    for activity in activities:
        data["distance"].append(activity["distance"])

    df = pd.DataFrame(data=data)
    fig = df.hist(nbins=5, color_discrete_sequence=[PRIMARY_COLOR])
    fig.update_layout(bargap=.1)

    fig.update_layout(xaxis={"title": "Distance"}, yaxis={"title": "# Runs"}, showlegend=False,
                      paper_bgcolor="rgba(0,0,0,0)", plot_bgcolor="rgba(0,0,0,0)")

    topOffset = PasteHeadlines(result, str(year), "RUNS")
    PasteFigure(fig, topOffset, result)

    result.save(IMAGES_DIR + "/distance-{}.png".format(year), "PNG")

    return result


def GetFormattedRunningStats(year) -> str:
    stats = GetStats(year)
    formatted = "{} RUNS | {} | {}".format(
        stats["countRuns"], stats["distanceRun"], stats["movingTimeRun"])
    return formatted


def RenderDistancePerMonth(year) -> Image:
    result = CreateBackground()

    df = GetDataFrameForActivities(year)
    fig = px.histogram(df, x="Month", y="Distance KM", histfunc="sum",
                       color_discrete_sequence=[PRIMARY_COLOR], text_auto=True)
    fig.update_layout(xaxis={"title": ""}, yaxis={"title": "Distance", "ticksuffix": "km"}, showlegend=False,
                      paper_bgcolor="rgba(0,0,0,0)", plot_bgcolor="rgba(0,0,0,0)")

    topOffset = PasteHeadlines(result, str(
        year), GetFormattedRunningStats(year), "run")
    PasteFigure(fig, topOffset, result)

    result.save(IMAGES_DIR + "/distance-per-month-{}.png".format(year), "PNG")

    return result


def RenderDistancePerGear(year) -> Image:
    result = CreateBackground()

    data = {
    }

    activities = GetActivities(DATA_DIR, year, type="Run")

    for activity in activities:
        gear = ""
        if "gear_id" in activity.keys():
            gear = activity["gear_id"]

        if gear not in data.keys():
            data[gear] = int(activity["distance"] / 1000)
        data[gear] = data[gear] + int(activity["distance"] / 1000)

    seriesData = {}

    for gearId in data.keys():
        value = data[gearId]
        fileName = DATA_DIR + "/gear-{}.json".format(gearId)
        if os.path.exists(fileName):
            gear = json.load(open(fileName))
            seriesData[gear["name"]] = value
        else:
            seriesData[gearId] = value

    df = pd.Series(seriesData).to_frame()
    fig = px.bar(df, color_discrete_sequence=[
                 PRIMARY_COLOR], orientation='h', text_auto=True)

    fig.update_layout(xaxis={"title": "", "ticksuffix": "km"}, yaxis={"title": "", "categoryorder": "total ascending"}, showlegend=False,
                      paper_bgcolor="rgba(0,0,0,0)", plot_bgcolor="rgba(0,0,0,0)")

    topOffset = PasteHeadlines(result, str(
        year), GetFormattedRunningStats(year), "run")
    PasteFigure(fig, topOffset, result)

    result.save(IMAGES_DIR + "/gear-{}.png".format(year), "PNG")

    return result


def RenderIntensityChart(year, activityType="Run") -> Image:
    result = CreateBackground()

    df = GetDataFrameForActivities(year, activityType=activityType)
    fig = px.scatter(
        x=df['Distance'],
        y=df['Elevation'],
        size=df['Duration'],
        color=df['Pace'],
        color_continuous_scale=MAIN_COLOR_SEQUENCE
    )

    fig.update_layout(
        xaxis={"title": "Distance", "ticksuffix": "", "showgrid": False},
        yaxis={"title": "Elevation Gain",
               "ticksuffix": " m", "showgrid": False},
        showlegend=False,
        coloraxis_showscale=True,
        coloraxis_colorbar_title_text='Avg. Pace<br />(min/km)',
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)")

    topOffset = PasteHeadlines(result, str(
        year), GetFormattedRunningStats(year), "run")
    PasteFigure(fig, topOffset, result)

    result.save(IMAGES_DIR + "/intensity-{}.png".format(year), "PNG")

    ApplyInensityClustering(df)

    result = CreateBackground()
    fig = px.scatter(
        x=df['Distance'],
        y=df['Elevation'],
        color=df['Intensity Cluster'],
        color_continuous_scale=MAIN_COLOR_SEQUENCE
    )

    fig.update_layout(
        xaxis={"title": "Distance", "ticksuffix": "", "showgrid": False},
        yaxis={"title": "Elevation", "ticksuffix": "", "showgrid": False},
        showlegend=False,
        coloraxis_showscale=True,
        coloraxis_colorbar_title_text='Intensity <br />Cluster <br />',
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)")

    topOffset = PasteHeadlines(result, str(
        year), GetFormattedRunningStats(year), "run")
    PasteFigure(fig, topOffset, result)

    result.save(IMAGES_DIR + "/intensity-clusters-{}.png".format(year), "PNG")

    return result


def GetDataFrameForActivities(year, forMonth="all", activityType="Run"):
    activityIds = []
    activityDates = []
    types = []
    durations = []
    distances = []
    elevations = []
    months = []
    monthNumbers = []
    averageHRs = []
    maxHRs = []
    paces = []
    timeOfDays = []
    startHours = []

    activities = GetActivities(DATA_DIR, year, forMonth, activityType)
    for activity in activities:
        activityIds.append(str(activity['id']))
        types.append(activity['type'])
        timeOfDays.append(activity['time_of_day'])
        startHours.append(activity['start_hour'])

        month = GetActivityMonthName(activity)
        months.append(month)

        monthNumber = GetActivityMonthNumber(activity)
        monthNumbers.append(monthNumber)

        duration = activity['moving_time']
        durations.append(duration)

        durationInMinutes = duration / 60

        distance = activity['distance']
        if distance != None and distance > 0:
            distances.append(distance)
            distanceInKM = distance / 1000
            paces.append(durationInMinutes / distanceInKM)
        else:
            distances.append(float('NaN'))
            paces.append(float('NaN'))

        activityDates.append(str(activity['start_date']))
        elevations.append(activity['total_elevation_gain'])

        if activity['has_heartrate'] == True:
            averageHRs.append(activity['average_heartrate'])
            maxHRs.append(activity['max_heartrate'])
        else:
            averageHRs.append(float("NaN"))
            maxHRs.append(float("NaN"))

    df = pd.DataFrame()
    df['ID'] = activityIds
    df['Start Hour'] = startHours
    df['Time of Day'] = timeOfDays
    df['Type'] = types
    df['Distance'] = distances
    df['Duration'] = durations
    df['Distance KM'] = round(df['Distance'] / 1000, 2)
    df['Elevation'] = elevations
    df['Month'] = months
    df['Month Number'] = monthNumbers
    df['Average HR'] = averageHRs
    df['Maximum HR'] = maxHRs
    df['Pace'] = paces

    return df


def GetDataFrameForActivityStreams(activity):
    df = pd.DataFrame()
    if not 'timeStream' in activity:
        return None

    df['Time'] = activity['timeStream']
    if 'distanceStream' in activity.keys():
        df['Distance'] = activity['distanceStream']

    if 'altitudeStream' in activity.keys():
        df['Altitude'] = activity['altitudeStream']

    if ('heartrateStream' in activity.keys()):
        df['HR'] = activity['heartrateStream']
        df['HR Smoothed'] = activity['heartrateStreamSmooth']

    return df


def ApplyClustering(df, columns, resultColumn, clusters=4, normalize=True):
    inputList = []
    for column in columns:
        if normalize:
            df[column + ' Normalized'] = MinMaxScaler().fit_transform(np.array(df[column]
                                                                               ).reshape(-1, 1))
            inputList.append(df[column + ' Normalized'])
        else:
            inputList.append(df[column])

    input = transpose(inputList)
    model = GaussianMixture(n_components=clusters)
    df[resultColumn] = model.fit_predict(input).astype(str)


def RenderHeartRateBoxPlots(year) -> Image:
    result = CreateBackground()
    df = GetDataFrameForActivities(year, "all", "Run")

    fig = make_subplots(specs=[[{"secondary_y": True}]])
    histogram = px.histogram(df, x="Month", y="Elevation",
                             histfunc="sum", color_discrete_sequence=["#efefef"])
    histogram.update_traces({'marker_line_color': '#efefef'})
    fig.add_traces(list(histogram.select_traces()), secondary_ys=[False])

    fig.add_traces(
        list(px.box(df, x="Month", y="Average HR", color_discrete_sequence=["blue"]).select_traces()), secondary_ys=[True])
    fig.add_traces(
        list(px.box(df, x="Month", y="Maximum HR",
                    color_discrete_sequence=["red"]).select_traces()), secondary_ys=[True])

    fig.update_layout(
        showlegend=False,
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)")

    fig.update_yaxes(title_text="Total Elevation Gain", secondary_y=False)
    fig.update_yaxes(
        title_text='<a style="color: blue">Average</a> / <a style="color: red">Max</a> Heartrate', secondary_y=True)
    fig.update_yaxes({'ticksuffix': 'm', 'showgrid': False}, secondary_y=False)

    topOffset = PasteHeadlines(
        result, str(year), GetFormattedRunningStats(year), "run")
    PasteFigure(fig, topOffset, result)

    result.save(
        IMAGES_DIR + "/heartrate-{}.png".format(year), "PNG")

    return result


def RenderActivityDetail(activityId: str) -> Image:
    result = CreateBackground()

    activity = GetActivity(DATA_DIR, activityId)

    if (activity == None):
        return result

    subHeadline = "{} | {}".format(GetActivityDate(
        activity), GetActivityMovingTime(activity))

    if 'distance' in activity:
        x = 'Distance'
        subHeadline += " | {0:.2f} km".format(activity['distance'] / 1000)

    topOffset = PasteHeadlines(
        result, activity['name'], subHeadline, activity['type'].lower())

    df = GetDataFrameForActivityStreams(activity)
    if df is None:
        return result

    x = "Time"

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    previous = 0
    index = 0

    for threshold in HR_ZONES:
        if previous > 0:
            lowerBound = threshold - previous
        else:
            lowerBound = threshold
        previous = threshold
        zone = np.full(shape=len(df[x]), fill_value=lowerBound)

        fill = 'none'
        if index > 0:
            fill = 'tonexty'

        fig.add_trace(go.Scatter(
            x=df[x], y=zone,
            mode='lines',
            line=dict(width=0),
            fill=fill,
            fillcolor=HR_ZONE_COLOR_SEQUENCE[index],
            stackgroup='one',
            legendgroup='zones',
        ))
        index += 1

    fig.update_traces(showlegend=False)

    fig.add_traces(
        list(
            px.line(df, x=x, y="HR Smoothed",
                    color_discrete_sequence=[HR_LINE_COLOR]).select_traces()
        )
    )

    if 'Altitude' in df.columns:
        traces = px.line(df, x=x, y="Altitude",
                         color_discrete_sequence=[ALTITUDE_LINE_COLOR]).select_traces()

        fig.add_traces(list(traces),secondary_ys=[True])

    fig.update_yaxes(patch={'title_text': 'HR', 'showgrid' : False}, secondary_y=False)
    fig.update_yaxes(patch={'title_text': 'Altitude', 'showgrid' : False}, secondary_y=True)

    fig.update_layout(
        paper_bgcolor="rgba(0,0,0,0)",
        plot_bgcolor="rgba(0,0,0,0)")

    PasteFigure(fig, topOffset, result)
    result.save(IMAGES_DIR + '/activity-{}.png'.format(activityId))
    return result

def ApplyInensityClustering(df):
    ApplyClustering(df, ['Distance', 'Elevation', 'Duration', 'Pace'],
                    'Intensity Cluster', 10, True)

# RenderComparison(2022, 2021)
# RenderOverview(2022)
# RenderOverview(2023)
# RenderActivityDistributionByMonth(2022)
# RenderActivityDistributionByTimeOfDay(2022)
# RenderDistanceHistogram(2022)
# RenderDistancePerMonth(2022)
# RenderHeartRateBoxPlots(2022)
RenderIntensityChart(2022).show()
# RenderDistancePerGear(2022)

# RenderActivityDetail('8319155934').show()
# RenderActivityDetail('8284255700').show()
# RenderActivityDetail('8345419023').show()
# RenderActivityDetail('8388193273').show()

df2022 = GetDataFrameForActivities(2022)
df2023 = GetDataFrameForActivities(2023)
df = pd.concat([df2022, df2023])

ApplyInensityClustering(df)

# ApplyClustering(df, ['Distance', 'Elevation'], 'Matched Run Cluster', 10)
cluster = df[df['ID'] == '8388193273']['Intensity Cluster'].iloc[0]
matchedRunIDs = df[df['Intensity Cluster'] == cluster]['ID'].array

print("> {} matched runs".format(len(matchedRunIDs)))

for id in matchedRunIDs:
    print("  >> {}".format(id))
    RenderActivityDetail(id).show()
