import os
import dateutil.parser
import json
import re
from scipy import signal

def AddTimeOfDay(activity: dict):
    hour = int(dateutil.parser.isoparse(
        activity["start_date_local"]).strftime('%H'))

    activity['start_hour'] = hour

    timeOfDay = None
    if (hour <= 11):
        timeOfDay = 'Morning'
    elif (hour > 11 and hour <= 13):
        timeOfDay = 'Noon'
    elif (hour > 13 and hour <= 17):
        timeOfDay = 'Afternoon'
    elif (hour > 17):
        timeOfDay = 'Evening'

    activity['time_of_day'] = timeOfDay


def GetActivities(dataDir: str, forYear: str, forMonth: str = "all", type: str = "all"):
    result = []
    for filename in sorted(os.listdir(dataDir)):
        f = os.path.join(dataDir, filename)

        matchesTimeFrame = False
        if forMonth == "all":
            matchesTimeFrame = filename.startswith("{}-".format(forYear))
        else:
            matchesTimeFrame = filename.startswith(
                "{}-{}".format(forYear, forMonth))

        if os.path.isfile(f) and filename.endswith(".json") and matchesTimeFrame:
            jsonData = json.load(open(f))

            month = "n/a"
            if (len(jsonData) > 0):
                month = dateutil.parser.isoparse(
                    jsonData[0]["start_date"]).strftime('%b')

            for activity in jsonData:
                if activity["type"] == type or type == "all":
                    result.append(activity)
                    AddTimeOfDay(activity)

    return result


def FindActivity(dataDir: str, activityId: str):
    result = None

    for filename in sorted(os.listdir(dataDir)):
        f = os.path.join(dataDir, filename)
        if os.path.isfile(f) and re.match(r"[0-9][0-9][0-9][0-9]-[0-9][0-9].json", filename):
            jsonData = json.load(open(f))

            for activity in jsonData:
                if str(activity['id']) == activityId:
                    AddTimeOfDay(activity)
                    result = activity
    return result


def GetActivity(dataDir: str, activityId: str):
    activity = FindActivity(dataDir, activityId)

    streamsFileName = os.path.join(
        dataDir, 'streams-{}.json'.format(activityId))
    if os.path.isfile(streamsFileName):
        jsonData = json.load(open(streamsFileName))

        timeStream = jsonData['time']
        activity['timeStream'] = timeStream['data']

        if (jsonData['heartrate'] != None):
            heartrateStream = jsonData['heartrate']
            activity['heartrateStream'] = heartrateStream['data']
            windowLength = int(len(activity['heartrateStream']) / 10)
            activity['heartrateStreamSmooth'] = signal.savgol_filter(heartrateStream['data'], window_length=windowLength, polyorder=3, mode="nearest")

        if ('distance' in jsonData.keys()):
            distanceStream = jsonData['distance']
            activity['distanceStream'] = distanceStream['data']

        if ('altitude' in jsonData.keys()):
            altitudeStream = jsonData['altitude']
            activity['altitudeStream'] = altitudeStream['data']

    lapsFileName = os.path.join(
        dataDir, 'laps-{}.json'.format(activityId))
    if os.path.isfile(lapsFileName):
        jsonData = json.load(open(lapsFileName))
        activity['laps'] = jsonData

    return activity
