import json
import datetime
import requests
import calendar
import os
from elasticsearch import Elasticsearch, helpers

STRAVA_CLIENT_ID = ''
STRAVA_CLIENT_SECRET = ''
STRAVA_REFRESH_TOKEN = ''

ELASTIC_URL = ''
ELASTIC_USER = ''
ELASTIC_PASSWORD = ''

with open('config/STRAVA_CLIENT_ID') as file:
    STRAVA_CLIENT_ID = file.read()
with open('config/STRAVA_CLIENT_SECRET') as file:
    STRAVA_CLIENT_SECRET = file.read()
with open('config/STRAVA_REFRESH_TOKEN') as file:
    STRAVA_REFRESH_TOKEN = file.read()
with open('config/ELASTIC_URL') as file:
    ELASTIC_URL = file.read()
with open('config/ELASTIC_USER') as file:
    ELASTIC_USER = file.read()
with open('config/ELASTIC_PASSWORD') as file:
    ELASTIC_PASSWORD = file.read()

ES_INDEX = 'strava'

stravaBaseUrl = "https://www.strava.com/api/v3/"

payload = ""

def AuthorizeWithCode(code):
    grantType = "authorization_code"

    url = "https://www.strava.com/oauth/token?client_id={}&client_secret={}&grant_type={}&code={}".format(
        STRAVA_CLIENT_ID, STRAVA_CLIENT_SECRET, grantType, code)
    data = json.loads(requests.request("POST", url).text)
    print(data)

    return data

def AuthorizeWithRefreshToken(refreshToken):
    grantType = "refresh_token"
    url = "https://www.strava.com/oauth/token?client_id={}&client_secret={}&grant_type={}&refresh_token={}".format(
        STRAVA_CLIENT_ID, STRAVA_CLIENT_SECRET, grantType, refreshToken)

    data = json.loads(requests.request("POST", url).text)
    print(data)

    return data

def DumpStravaActivities(after, before, fileName, includeDetails):
    url = stravaBaseUrl + "athlete/activities"
    querystring = {"per_page": "50", "page": "1",
                   "after": after, "before": before}
    activities = json.loads((requests.request(
        "GET", url, data=payload, headers=stravaHeaders, params=querystring).text))

    print("DumpStravaActivities: {} -> {} activities".format(fileName, len(activities)))

    with open('data/{}'.format(fileName), 'w') as outfile:
        outfile.write(json.dumps(activities))

    for activity in activities:
        if "gear_id" in activity.keys():
            DumpGearDetails(activity['gear_id'])

        if includeDetails == True:
            DumpStravaLaps(activity['id'])
            DumpStravaActivityStreams(activity['id'])

def DumpGearDetails(gearId):
    fileName = 'data/gear-{}.json'.format(gearId)
    if not os.path.exists(fileName):
        url = stravaBaseUrl + "gear/{}".format(gearId)
        gearJson = json.loads((requests.request("GET", url, data=payload,
                                                headers=stravaHeaders).text))
        with open(fileName, 'w') as outfile:
            outfile.write(json.dumps(gearJson))

def DumpStravaLaps(activityId):
    querystring = {}
    url = stravaBaseUrl + 'activities/{}/laps'.format(activityId)
    laps = json.loads((requests.request("GET", url, data=payload,
                                        headers=stravaHeaders, params=querystring).text))
    with open('data/laps-{}.json'.format(activityId), 'w') as outfile:
        outfile.write(json.dumps(laps))

def DumpStravaActivityStreams(activityId):
    querystring = {
        "keys": "heartrate,time,altitude,latlng,distance",
        "key_by_type": "true"
    }
    url = stravaBaseUrl + \
        'activities/{}/streams'.format(
            activityId)
    streams = json.loads((requests.request("GET", url, data=payload,
                                           headers=stravaHeaders, params=querystring).text))
    fileName = 'data/streams-{}.json'.format(activityId)
    with open(fileName, 'w') as outfile:
        print("  DumpStravaActivityStreams: {} -> {} streams ({})".format(fileName,
              len(streams), list(streams.keys())))

        outfile.write(json.dumps(streams))

def GetActivityDocs(activities):
    for activity in activities:
        doc = {
            "_index": ES_INDEX,
            "_id": activity['id'],
            "_source": {
                "@timestamp": datetime.datetime.strptime(activity['start_date'].replace('Z', '')+activity['timezone'][4:10], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%dT%H:%M:%S%z'),
                "strava": activity,
                "data": {}
            }
        }
        yield doc

def GetLapDocs(laps):
    for lap in laps:
        doc = {
            "_index": ES_INDEX,
            "_id": lap['id'],
            "_source": {
                "@timestamp": datetime.datetime.strptime(lap['start_date'].replace('Z', '')+lap['timezone'][4:10], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%dT%H:%M:%S%z'),
                "strava_lap": lap,
                "data": {}
            }
        }
        yield doc

def ImportActivities(activities):
    client = Elasticsearch(
        [ELASTIC_URL],
        verify_certs=False,
        basic_auth=(ELASTIC_USER, ELASTIC_PASSWORD)
    )
    docs = GetActivityDocs(activities)
    helpers.bulk(client, docs)

def DumpStravaActivitiesForYears(years, includeDetails):
    for year in years:
        for index in range(0, 12):
            month = index + 1
            DumpStravaActivitiesForMonth(year, month, includeDetails)


def DumpStravaActivitiesForMonth(year, month, includeDetails):
    last = calendar.monthrange(year, month)[1]
    filename = "{}-{:02d}.json".format(year, month)

    after = datetime.datetime(year, month, 1, 0, 0, 0).strftime('%s')
    before = datetime.datetime(
        year, month, last, 23, 59, 0).strftime('%s')
    DumpStravaActivities(after, before, filename, includeDetails)


def ImportActivitiesFromDirectory(dir):
    for filename in os.listdir(dir):
        f = os.path.join(dir, filename)

        if (os.path.isfile(f)
                and filename.endswith(".json") and (not filename.startswith("laps-") and not filename.startswith("streams-") and not filename.startswith("gear-"))):
            ImportActivities(json.load(open(f)))

authData = AuthorizeWithRefreshToken(STRAVA_REFRESH_TOKEN)
stravaHeaders = {
    "Authorization": "Bearer {}".format(authData['access_token'])
}

# DumpStravaActivitiesForYears([2022], False)
DumpStravaActivitiesForMonth(2022, 6, True)
# DumpStravaActivitiesForMonth(2023, 1, True)
# ImportActivities(json.load(open("data/2022-12.json")))
# ImportActivitiesFromDirectory("data")
